        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-8 col-md-offset-2">
                            <h1>Using sonification in biology pedagogy</h1>
                            <p>Does music help students remeber biological elements? 
                              In the summer of 2014 a small research team investigated the educational effectiveness sonified scientific data presented to students enrolled in general education biology course. This site holds what we learned.</p>
                            <p><button type="button" class="btn btn-primary btn-lg">SEE RESULTS</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->